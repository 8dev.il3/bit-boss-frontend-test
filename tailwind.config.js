/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            fontFamily: {
                'sans': 'Work sans, Helvetica, Arial, sans-serif'
            },
            boxShadow: {
                'desktop': '-4px 4px 8px #00000033',
                'mobile': '-4px 4px 4px #00000033'
            }
        },
    },
    plugins: [],
}

