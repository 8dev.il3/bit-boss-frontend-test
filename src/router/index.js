import { createRouter, createWebHistory } from 'vue-router'
import Index from '../views/Index.vue';
import Profile from '../views/Profile.vue';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Index
        },
        {
            path: '/profile/:id',
            name: 'profile',
            component: Profile
        },
    ]
});

export default router
